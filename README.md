# HUGB 23 G 23

This is a README file for group 23 in T-303-HUGB at Reykjavík University on the fall semester 2023. The members of the group are Dana Sól Tryggvadóttir, Eiríkur Fannar Jónsson, Hlín Eiríksdóttir, Illugi Dreki Finsson, Ingólfur Hannes Leósson og Sunna Káradóttir.

## Description

The goal of this projects is to implement a software called Travelator. A software system that allows to plan for travels, by defining
favourite places or planned spots to visit, complete with map coordinates. Advanced functions will allow for an amazing travel experience. The main focus in the work is on everything around the software, such as testing, documentation and processes, and the groups doucmentation can be found in this git repository as well as the code itself. The project is being done in sprints and currently two sprints out of five have been done. 

## Running the application

# Running requirements.txt

Run the following command from your terminal inside HUGB_23_G_23:

pip (or pip3) install -r requirements.txt

# Grpc
To compile proto files:
python3 -m grpc_tools.protoc -I <directory of proto file> --python_out=<directory of pb2.py file> --grpc_python_out=<directory of pb2_grpc.py file> <.proto filename>
Run the following commands from your terminal (inside HUGB_23_G_23/src folder)

- start the server
py (or py3) ./src/python/server.py

- start the client
py (or py3) ./src/python/client.py

# Running the main program

Run the following command from your terminal inside HUGB_23_G_23:

""" main file needs to be reviewed """

""" detailed steps about how to run the final product --> needs to be reviewed later """

# Running the tests

To run the tests you have to be located in the src/python folder

There you can run the command:

make test  - this will test the program

make coverage - this will give you a coverage report to see how much of the program the tests cover and see the results in a file called coverage_report.txt

## Installation

The software system is implemented in python. It is implemented with the GRPC framework from Google and the data is stored in json files.

## Usage

Travelator is a software system that allows to plan for travels, by defining
favourite places or planned spots to visit, complete with map coordinates.
Advanced functions will allow for an amazing travel experience. (Site: project description on Canvas)

## Support
Contact any of the group members for help

## Authors and acknowledgment
- Dana Sól Tryggvadóttir
- Eiríkur Fannar Jónsson 
- Hlín Eiríksdóttir
- Illugi Dreki Finsson
- Ingólfur Hannes Leósson
- Sunna Káradóttir - Scrum Master for sprint 3


## Project status
In progress.
