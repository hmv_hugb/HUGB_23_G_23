from concurrent import futures

import grpc

import travel_pb2
import travel_pb2_grpc


from interfaces.travel_interface import TravelInterface


class Travelator(travel_pb2_grpc.TravelatorServicer):
    def __init__(self):
        super().__init__()  # Call the __init__ method of the parent class
        self.iface = TravelInterface()

    def get_place(self, request, context):
        """Get a place from the server"""
        place_id = request.id
        place = self.iface.get_place(place_id)
        if place is not None:
            # print(f"Received following from server: Name: {response.name}, Description: {response.description}, Location: {response.location}")
            return travel_pb2.PlaceResponse(
                name=place.name, description=place.description, location=place.location
            )
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details(f"Place with id {place_id} not found")
            return travel_pb2.PlaceResponse()

    def add_place(self, request, context):
        """Add a place to the server"""
        name = request.name
        description = request.description
        location = request.location

        if not name or not location:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("Name and location are required")
            return travel_pb2.AddPlaceResponse(success=False, id=0)

        new_place = self.iface.add_place(name, description, location)
        # printing out success message with the new added place with name, description and location
        print(f"\nPlace added successfully.")
        print("---------------------------")
        print(
            f"Name: {new_place.name}, \nDescription: {new_place.description}, \nLocation: {new_place.location}"
        )
        return travel_pb2.AddPlaceResponse(success=True, id=new_place.id)


def server():
    """Start the gRPC server"""
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
    travel_pb2_grpc.add_TravelatorServicer_to_server(Travelator(), server)
    server.add_insecure_port("[::]:50051")
    print("gRPC starting")
    server.start()
    server.wait_for_termination()


server()
