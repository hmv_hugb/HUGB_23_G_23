import unittest
from registration_login import UserManager

class TestUserManager(unittest.TestCase):

    def setUp(self):
        self.user_manager = UserManager()

    def test_registration_successful(self):
        result = self.user_manager.register("testuser", "password123")
        self.assertEqual(result, "Registration successful!")

    def test_registration_duplicate_username(self):
        self.user_manager.register("testuser", "password123")
        result = self.user_manager.register("testuser", "anotherpassword")
        self.assertEqual(result, "Username already exists. Please choose a different username.")

    def test_login_invalid_username(self):
        result = self.user_manager.login("nonexistentuser", "password123")
        self.assertEqual(result, "Username does not exist.")

    def test_login_invalid_password(self):
        self.user_manager.register("testuser", "password123")
        result = self.user_manager.login("testuser", "wrongpassword")
        self.assertEqual(result, "Invalid login details")

if __name__ == "__main__":
    unittest.main()
