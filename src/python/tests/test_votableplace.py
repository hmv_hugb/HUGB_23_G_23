
import unittest

from interfaces.travel_interface import VotablePlace

# TODO: to run tests: python3 -m unittest tests/test_place.py
from models.place import Location




class TestVotablePlace(unittest.TestCase):
    def setUp(self):
        """Setting up two VotablePlace objects for testing"""
        self.votable_place = VotablePlace(
            id=1,
            name="Hotel transylvania",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=23.6345,
                longitude=102.5528,
            ),
            votes=0,
        )

        self.votable_place2 = VotablePlace(
            id=2,
            name="Disney Land",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=23.6345,
                longitude=102.5528,
            ),
            votes=0,
        )

        return super().setUp()

    def test_upvote(self):
        """testing upvote method"""
        self.votable_place.upvote()
        self.assertEqual(self.votable_place.votes, 1)

    def test_str_with_votes(self):
        """testing str method with votes"""
        self.votable_place.upvote()
        self.votable_place.upvote()
        self.assertIn("Votes: 2", str(self.votable_place))

    def test_str_without_votes(self):
        """testing str method without votes"""
        self.assertIn("Votes: 0", str(self.votable_place))

    def test_multiple_upvotes(self):
        """testing multiple upvotes"""
        for _ in range(5):
            self.votable_place.upvote()
        self.assertEqual(self.votable_place.votes, 5)

    def test_mixed_voting(self):
        """testing mixed voting, for example upvote, upvote, downvote. Votes should be 1."""
        self.votable_place.upvote()
        self.votable_place.upvote()
        self.votable_place.downvote()
        self.assertEqual(self.votable_place.votes, 1)

    def test_votes_dont_go_below_zero(self):
        """testing that votes don't go below zero, should be 0 after 3 downvotes"""
        for _ in range(3):
            self.votable_place.downvote()
        self.assertEqual(self.votable_place.votes, 0)

    def test_voting_on_multiple_places(self):
        """testing voting on multiple places, should be 1 after 1 upvote"""
        self.votable_place.upvote()
        self.votable_place2.upvote()
        self.votable_place2.downvote()
        self.votable_place.upvote()
        self.assertEqual(self.votable_place2.votes, 0)
        self.assertEqual(self.votable_place.votes, 2)
