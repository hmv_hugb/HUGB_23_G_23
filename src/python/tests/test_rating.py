import unittest
from models.place import PlaceRating

class TestPlaceRating(unittest.TestCase):

    def setUp(self):
        """Setting up test data"""
        self.user_id = 1
        self.rating_value = 4
        self.review_text = None
        self.place_rating = PlaceRating(self.user_id, self.rating_value, self.review_text)

    def test_create_rating(self):
        # Check if the rating was created correctly
        self.assertEqual(self.place_rating.user_id, self.user_id)
        self.assertEqual(self.place_rating.rating, self.rating_value)
        self.assertEqual(self.place_rating.review, self.review_text)

    def test_invalid_user_id(self):
        # Assert an error is raised when providing a non-integer user_id
        with self.assertRaises(TypeError):
            PlaceRating("user", self.rating_value, self.review_text)

    def test_user_id_does_not_exist(self):
        # Assert an error is raised when providing a non-integer user_id
        with self.assertRaises(ValueError):
            PlaceRating(4, self.rating_value, self.review_text)

    def test_none_user_id(self):
        # Assert an error is raised when providing a non-integer user_id
        with self.assertRaises(TypeError):
            PlaceRating(None, self.rating_value, self.review_text)

    def test_invalid_rating(self):
        # Assert an error is raised when providing an invalid rating
        with self.assertRaises(ValueError):
            PlaceRating(self.user_id, 6, self.review_text)

        with self.assertRaises(ValueError):
            PlaceRating(self.user_id, 11, self.review_text)
        
        with self.assertRaises(ValueError):
            PlaceRating(self.user_id, -1, self.review_text)
        

    def test_valid_rating(self):
        # Assert no error is raised when providing a valid rating
        PlaceRating(self.user_id, 1, self.review_text)
        PlaceRating(self.user_id, 5, self.review_text)
        PlaceRating(self.user_id, 1, self.review_text)


    def test_rating_equality(self):
        #Assert that two ratings with the same values are equal
        self.assertEqual(self.place_rating, PlaceRating(self.user_id, self.rating_value, self.review_text))


    def test_empty_review(self):
        #Assert that the review is None when no review is provided
        rating = PlaceRating(self.user_id, self.rating_value, "")
        self.assertEqual(rating.review, "")

    def test_rating_to_string(self):
        #Assert that the __str__ method returns the correct string
        self.assertEqual(str(self.place_rating), f"Rating of {self.rating_value}/5 by User {self.user_id}.")


    def test_very_long_review(self):
        #Assert that the review is truncated when it is too long
        long_review = "word " * 301
        with self.assertRaises(ValueError):
            PlaceRating(self.user_id, self.rating_value, long_review)

    def test_minimum_word_review(self):
        #Assert that the review is not truncated when it is 300 words long
        long_review = "word " * 300
        PlaceRating(self.user_id, self.rating_value, long_review)

    def test_none_rating(self):
        #Assert that the rating is 0 when no rating is provided
        with self.assertRaises(ValueError):
            PlaceRating(self.user_id, None, self.review_text)

    def test_rating_inequality(self):
        rating1 = PlaceRating(self.user_id, 2, "Nice place!")
        rating2 = PlaceRating(self.user_id, 5, "Excellent!")
        self.assertNotEqual(rating1, rating2)

    def test_rating_with_review_to_string(self):
        review = "Amazing place!"
        rating = PlaceRating(self.user_id, self.rating_value, review)
        self.assertEqual(str(rating), f"Rating of {self.rating_value}/5 by User {self.user_id}. Review: {review}")

    def test_float_rating(self):
        with self.assertRaises(ValueError):
            PlaceRating(self.user_id, 3.5, self.review_text)



