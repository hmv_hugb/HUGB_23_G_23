import unittest

from interfaces.travel_interface import TravelInterface

# TODO: to run tests: python3 -m unittest tests/test_place.py
from models.place import Place, Location


class TestPlace(unittest.TestCase):
    def setUp(self) -> None:
        """Setting up sample Place objects for testing."""
        # Initialize Place objects for tests
        self.place1 = Place(
            id=1,
            name="Hotel transylvania",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=23.6345,
                longitude=102.5528,
            ),
        )
        self.place2 = Place(
            id=1,
            name="Hotel transylvania",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=23.6345,
                longitude=102.5528,
            ),
        )
        self.place3 = Place(
            id=11,
            name="Worldfit Kringlan",
            description="Gym for crazy people",
            location=Location(
                latitude=64.07504,
                longitude=21.53495,
            ),
        )

        return super().setUp()

    def test_place_eq_correct(self):
        """Test for equality of two similar Place objects."""
        self.assertEqual(self.place1, self.place2)

    def test_place_eq_not_correct(self):
        """Test for inequality of two dissimilar Place objects."""
        self.assertNotEqual(self.place1, self.place3)

    def test_place_eq_not_same_object(self):
        """Test for inequality when compared with different object type."""
        self.assertNotEqual(self.place1, ("fake", "place"))

    def test_place_location_eq_not_same_object(self):
        """Test for inequality of Location when compared with different object type."""
        self.assertNotEqual(
            Location(
                latitude=64.07504,
                longitude=21.53495,
            ),
            (4, 5),
        )


class TestTravelInterface(unittest.TestCase):
    def setUp(self):
        """Setting up a TravelInterface object for testing."""
        # Initialize TravelInterface for tests
        self.interface = TravelInterface()
        self.interface.places = [
            Place(
                1,
                "Hotel transylvania",
                "Bloodie vampiros de lagoon",
                Location(23.6345, 102.5528),
            )
        ]

    def test_get_place_by_id(self):
        """Test to fetch a place by its ID."""
        place = self.interface.get_place(1)
        expected_ret_place = Place(
            id=1,
            name="Hotel transylvania",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=23.6345,
                longitude=102.5528,
            ),
        )
        self.assertIsNotNone(place)
        self.assertEqual(place, expected_ret_place)

    def test_get_place_by_invalid_id(self):
        """Test to fetch a place by an invalid ID."""
        place = self.interface.get_place(80)
        self.assertIsNone(place)

    def test_create_place(self):
        """Test for creating a new place."""
        expected_place = Place(
            id=2,
            name="Worldfit Kringlan",
            description="Gym for crazy people",
            location=Location(
                latitude=64.07504,
                longitude=21.53495,
            ),
        )
        place = self.interface.add_place(
            "Worldfit Kringlan",
            "Gym for crazy people",
            Location(
                latitude=64.07504,
                longitude=21.53495,
            ),
        )
        self.assertEqual(place, expected_place)

    def test_get_all_places(self):
        """Test to fetch all places."""
        all_places = self.interface.get_all_places()
        self.assertEqual(
            all_places,
            [
                Place(
                    id=1,
                    name="Hotel transylvania",
                    description="Bloodie vampiros de lagoon",
                    location=Location(
                        latitude=23.6345,
                        longitude=102.5528,
                    ),
                )
            ],
        )

    def test_delete_place(self):
        """Test the delete_place function by adding a place, deleting it,
        and then verifying that it no longer exists."""
        # Adding a place to delete later
        self.interface.add_place(
            "Test Place",
            "A place for testing",
            Location(
                latitude=40.7128,
                longitude=74.0060,
            ),
        )

        # Verifying that the place was added
        self.assertIsNotNone(self.interface.get_place(2))

        # Deleting the place
        self.interface.delete_place(2)

        # Verifying that the place was deleted
        self.assertIsNone(self.interface.get_place(2))

    def test_delete_place_invalid_id(self):
        """Test the delete_place function with an invalid ID,
        and verify that it doesn't affect existing data."""
        # Trying to delete a place with an invalid ID
        self.interface.delete_place(999)

        # Verifying that the place was not deleted
        self.assertIsNone(self.interface.get_place(999))

    def test_z_edit_place(self):
        """This test is last (z) because it changes the places.json file
        and the other tests depend on the data in the file. Test for editing an exsisting place
        """
        expected_place = Place(
            id=1,
            name="Disney Land",
            description="Bloodie vampiros de lagoon",
            location=Location(
                latitude=23.6345,
                longitude=102.5528,
            ),
        )
        place = self.interface.edit_place(
            1,
            "Disney Land",
            "Bloodie vampiros de lagoon",
            Location(
                latitude=23.6345,
                longitude=102.5528,
            ),
        )
        self.assertEqual(place, expected_place)

    def test_load_travel_history(self):
        """Test that the travel_history attribute is loaded from a JSON file and is not None."""
        self.assertIsNotNone(self.interface.travel_history)

    def test_get_visited_places(self):
        """Test that get_visited_places() returns a list of Place objects corresponding to visited places."""
        visited = self.interface.get_visited_places()
        self.assertEqual(type(visited), list)

    def test_suggest_places(self):
        """Test that suggest_places() returns a list of Place objects that have not been visited."""
        suggestions = self.interface.suggest_places()
        self.assertEqual(type(suggestions), list)

    def test_share_place_on_social_media(self):
        place_id = 1
        shared_content = self.interface.share_on_social_media(place_id, "place")
        self.assertIn("Check out this place:", shared_content)

    def test_share_invalid_id(self):
        with self.assertRaises(Exception):
            self.interface.share_on_social_media(999, "place")

    def test_invalid_shareable_type(self):
        with self.assertRaises(ValueError):
            self.interface.share_on_social_media(1, "invalid_type")
