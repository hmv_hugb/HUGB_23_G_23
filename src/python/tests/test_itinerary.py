import unittest
import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
from models.itinerary import Itinerary
from models.place import Place, Location


class TestItinerary(unittest.TestCase):
    def setUp(self):
        self.location1 = Location(65.0, 18.0)
        self.place1 = Place(1, "Place 1", "Description 1", self.location1)

        self.location2 = Location(66.0, 19.0)
        self.place2 = Place(2, "Place 2", "Description 2", self.location2)

        self.itinerary = Itinerary("Sample Itinerary", "2023-01-01", "2023-01-10")

    def test_add_place(self):
        self.itinerary.add_place(self.place1)
        self.assertIn(self.place1, self.itinerary.list_places())

    def test_add_duplicate_place(self):
        self.itinerary.add_place(self.place1)
        with self.assertRaises(ValueError):
            self.itinerary.add_place(self.place1)

    def test_remove_place(self):
        self.itinerary.add_place(self.place1)
        self.itinerary.remove_place(1)
        self.assertNotIn(self.place1, self.itinerary.list_places())

    def test_remove_place_invalid_id(self):
        self.itinerary.add_place(self.place1)
        result = self.itinerary.remove_place(999)  # An ID not in the itinerary
        self.assertFalse(result)

    def test_list_places(self):
        self.itinerary.add_place(self.place1)
        self.itinerary.add_place(self.place2)

        places = self.itinerary.list_places()
        self.assertEqual(len(places), 2)
        self.assertIn(self.place1, places)
        self.assertIn(self.place2, places)

    def test_get_place_by_id(self):
        self.itinerary.add_place(self.place1)
        retrieved_place = self.itinerary.get_place(1)
        self.assertEqual(retrieved_place, self.place1)

    def test_change_visit_time(self):
        from datetime import datetime

        self.itinerary.add_place(self.place1)
        new_time = datetime.strptime("2023-01-02 14:30", "%Y-%m-%d %H:%M")
        self.itinerary.change_visit_time(1, new_time)
        self.assertEqual(self.itinerary.visit_times[1], new_time)

    def test_total_places(self):
        self.itinerary.add_place(self.place1)
        self.itinerary.add_place(self.place2)
        self.assertEqual(self.itinerary.total_places(), 2)

    def test_itinerary_str(self):
        self.itinerary.add_place(self.place1)
        expected_str = "Itinerary 'Sample Itinerary':\nPlace 1: Place 1 - Description 1 - 65.0 - 18.0"
        self.assertEqual(str(self.itinerary), expected_str)


if __name__ == "__main__":
    unittest.main()
