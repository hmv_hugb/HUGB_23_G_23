import unittest

from interfaces.user_interface import UserInterface


# TODO: to run tests: python3 -m unittest tests/test_place.py
from models.user import User


class TestUserInterface(unittest.TestCase):
    def setUp(self) -> None:
        self.interface = UserInterface()
        self.interface.users = [
            User(
                usr_id=1,
                name="Gerald Dorsett",
                birthdate="25.09.2000",
                email="g.dorsett@gmail.com",
            )
        ]

    def test_get_user_by_id(self):
        user = self.interface.get_user(1)
        exp_user = User(
            usr_id=1,
            name="Gerald Dorsett",
            birthdate="25.09.2000",
            email="g.dorsett@gmail.com",
        )
        self.assertIsNotNone(user)
        self.assertEqual(user, exp_user)

    def test_get_user_by_invalid_id(self):
        self.assertIsNone(self.interface.get_user(999))

    def test_create_user(self):
        exp_user = User(
            usr_id=2,
            name="Neal Caffery",
            birthdate="1.4.1995",
            email="neal.caffery@fbi.gov.com",
        )
        user = self.interface.add_user(
            name="Neal Caffery", birthdate="1.4.1995", email="neal.caffery@fbi.gov.com"
        )
        self.interface.update_users_json()
        self.assertIsNotNone(user)
        self.assertEqual(user, exp_user)

    def test_delete_user(self):
        self.interface.add_user(name="Gluggi", birthdate="24.01.2014", email="g@g.is")
        self.assertIsNotNone(self.interface.get_user(2))
        self.interface.delete_user(2)
        self.assertIsNone(self.interface.get_user(2))

    def test_delete_user_invalid_id(self):
        """Test the delete_place function with an invalid ID,
        and verify that it doesn't affect existing data."""
        # Trying to delete a place with an invalid ID
        self.interface.delete_user(999)

        # Verifying that the place was not deleted
        self.assertIsNone(self.interface.get_user(999))
