import getpass
import random
import hashlib
from typing import Dict, List
from models.user import User

class UserManager:
    """A simple class for user management, allowing user registration and login."""

    def __init__(self):
        """Initializes a new instance of the UserManager class, setting up an empty user database."""
        self.users_db: Dict[str, str] = {}  # Store hashed passwords
        self.user_objects: List[User] = []  # Store User objects

    def register(self, username: str, password: str) -> str:
        """
        Registers a new user with the given username and password.
        """
        if username in self.users_db:
            return "Username already exists. Please choose a different username."

        # Hash and salt the password before storing it
        hashed_password = hash_password(password)
        self.users_db[username] = hashed_password

        # Create a User object with placeholder values for usr_id and birthdate
        new_user = User(usr_id=-1, birthdate="", name=username, email="")  
        self.user_objects.append(new_user)

        return "Registration successful!"

    def login(self, username: str, password: str) -> str:
        """
        Logs in a user with the given username and password.
        """
        if username not in self.users_db:
            return "Username does not exist."

        # Verify the password using the stored hashed password
        stored_hashed_password = self.users_db[username]
        if verify_password(password, stored_hashed_password):
            return "Login successful!"
        else:
            return "Invalid login details"



def hash_password(password: str) -> str:
    """Hash and salt the password."""

    salt = str(random.randint(100000, 999999))  # Generate a random salt
    salted_password = password + salt
    hashed_password = hashlib.sha256(salted_password.encode()).hexdigest()
    return hashed_password

def verify_password(input_password: str, stored_hashed_password: str) -> bool:
    """Verify the input password against the stored hashed password."""

    salt = stored_hashed_password[-6:]  # Extract the salt
    salted_password = input_password + salt
    hashed_password = hashlib.sha256(salted_password.encode()).hexdigest()
    return hashed_password == stored_hashed_password

# Rest of your code remains the same



def main():
    """
    The main function of the script, which initializes a UserManager instance and prompts the user
    to either register a new user, log in an existing user, or exit the application.
    """
    user_manager = UserManager()
    while True:
        action = input(
            "Do you want to login, register or recover your password? (login/register/recover/exit): "
        ).lower()
        if action == "exit":
            break
        username = input("Enter your username: ")
        password = getpass.getpass("Enter your password: ")

        if action == "login":
            print(user_manager.login(username, password))
        elif action == "register":
            print(user_manager.register(username, password))
        elif action == "recover":
            choice = input(
                "Generate recovery code or reset password? (generate/reset): "
            ).lower()
            if choice == "generate":
                print(user_manager.generate_recovery_code(username))
            elif choice == "reset":
                recovery_code = int(input("Enter your recovery code: "))
                new_password = getpass.getpass("Enter your new password: ")
                print(
                    user_manager.reset_password(username, recovery_code, new_password)
                )
        else:
            print("Invalid action. Please enter 'login' or 'register' or 'exit'.")


if __name__ == "__main__":
    main()
