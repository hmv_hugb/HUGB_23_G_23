from enum import Enum
from typing import List


class InvalidRatingError(ValueError):
    """Raised when an invalid rating is provided"""
    pass

class NoRatingsError(ValueError):
    """Raised when no ratings are provided"""
    pass

class RatingMustBeInt(ValueError):
    """Raised when rating is not an integer"""
    pass

class NoNumberError(TypeError):
    """Raised when user_id is not a number"""
    pass

class InvalidUserIdError(ValueError):
    """Raised when user_id is not a valid user ID"""
    pass



class Location:
    def __init__(
        self, latitude: float, longitude: float
    ):
        if latitude>90 or latitude <-90 or longitude>180 or longitude <-180:
            raise ValueError("Invalid latitude or longitude.")
        self.latitude = latitude
        self.longitude = longitude

    def __eq__(self, __value: object) -> bool:
        '''Compare two Location objects for equality'''
        if __value is None: #exception handling
            return False

        if isinstance(__value, Location):
            return (
                self.latitude == __value.latitude
                and self.longitude == __value.longitude
            )
        return False
    
    def __str__(self):
        '''Return a string representation of the Location object'''
        return f'Location: {self.latitude} - {self.longitude}'
    
    def __repr__(self):
        '''Return a string representation of the Location object'''
        return self.__str__()


class Place:
    def __init__(self, id: int, name: str, description: str, location: Location):
        #exception handling
        if not isinstance(id, int) or not isinstance(name, str) or not isinstance(description, str) or not isinstance(location, Location): 
            raise ValueError("Invalid argument type.")

        self.id = id
        self.name = name
        self.description = description
        self.location = location

        self.ratings: List[PlaceRating] = []


    def __eq__(self, __value: object) -> bool:
        '''Compare two Place objects for equality'''
        if __value is None: #exception handling
            return False

        if isinstance(__value, Place):
            # Compare all attributes for equality
            return (
                self.id == __value.id
                and self.name == __value.name
                and self.description == __value.description
                and self.location == __value.location
            )
        return False
    
    def __str__(self):
        '''Return a string representation of the Place object'''
        return f'Place {self.id}: {self.name} - {self.description} - {self.location.latitude} - {self.location.longitude}'
    
    def __repr__(self):
        '''Return a string representation of the Place object'''
        return self.__str__()

    def add_rating(self, user_id: int, rating: int, review: str = None):
        """Adds a rating to a place"""
        new_rating = PlaceRating(user_id, rating, review)
        self.ratings.append(new_rating)
    
    def get_average_rating(self) -> int:
        """Returns the average rating of a place"""
        if not self.ratings:
            return 0
        total = sum([rating.rating for rating in self.ratings])
        return total / len(self.ratings)
    
    def get_all_ratings(self) -> list:
        """Returns all ratings of a place"""
        return self.ratings







VALID_USER_IDS = [1, 2, 3]


class PlaceRating:
    """Represents a rating for a place."""
    def __init__(self, user_id: int, rating: int, review: str = None):
        """Ensure that the rating is between 0 and 5 (inclusive)."""
        self.user_id = user_id
        self.rating = rating
        self.review = review
        
        if rating is None:
            raise NoRatingsError("Rating must be provided.")
        
        if not isinstance(rating, int):
            raise RatingMustBeInt("Rating must be a Integer.")
        
        if not isinstance(user_id, int):
            raise NoNumberError("User ID must be a number.")
        
        if not (0 <= rating <= 5):
            raise InvalidRatingError("Rating must be between 0 and 5 (inclusive).")
        
        if user_id not in VALID_USER_IDS:
            raise InvalidUserIdError("Not a valid user ID.")
        
        if review and len(self.review.split()) > 300:
            raise ValueError("Review must be less than 300 characters.")
        
        self.rating = rating
        

    def __eq__(self, __value: object) -> bool:
        '''Compare two PlaceRating objects for equality'''
        if __value is None:
            return False

        if isinstance(__value, PlaceRating):
            # Compare all attributes for equality
            return (
                self.user_id == __value.user_id
                and self.rating == __value.rating
                and self.review == __value.review
            )
        return False

        
    def __str__(self):
        """Returns a string representation of the rating"""
        review_text = f" Review: {self.review}" if self.review else ""
        return f"Rating of {self.rating}/5 by User {self.user_id}.{review_text}"


