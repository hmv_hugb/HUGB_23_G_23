from typing import List, Dict, Optional
from datetime import datetime
from models.place import Place

class Itinerary:
    def __init__(self, name: str, start_date: str, end_date: str):
        if datetime.strptime(start_date, "%Y-%m-%d") > datetime.strptime(end_date, "%Y-%m-%d"):
            raise ValueError("Start date must be before end date.")
        
        self.name = name
        self.start_date = start_date
        self.end_date = end_date
        self.places: Dict[int, Place] = {}
        self.visit_times: Dict[int, Optional[datetime]] = {}

    def add_place(self, place: Place, visit_time: Optional[datetime] = None):
        """Add a place to the itinerary"""
        if place.id in self.places:
            raise ValueError(f"Place with ID {place.id} already exists in the itinerary.")
        
        # Add checks for date overlap if required
        
        self.places[place.id] = place
        self.visit_times[place.id] = visit_time

    def remove_place(self, place_id: int) -> bool:
        """Remove a place from the itinerary"""
        if place_id in self.places:
            del self.places[place_id]
            del self.visit_times[place_id]
            return True
        return False

    def list_places(self) -> List[Place]:
        """List all places in the itinerary"""
        return list(self.places.values())

    def get_place(self, place_id: int) -> Optional[Place]:
        """Retrieve a place by its ID"""
        return self.places.get(place_id)

    def change_visit_time(self, place_id: int, new_time: Optional[datetime]):
        """Change the visit time of a place"""
        if place_id not in self.places:
            raise ValueError(f"No place with ID {place_id} found in the itinerary.")
        
        self.visit_times[place_id] = new_time

    def total_places(self) -> int:
        """Return the total number of places in the itinerary"""
        return len(self.places)

    def __str__(self) -> str:
        """Return a string representation of the itinerary"""
        place_details = "\n".join([
            f"{place}{' at ' + str(self.visit_times[place.id]) if self.visit_times.get(place.id) else ''}" 
            for place in self.places.values()
        ])
        return f"Itinerary '{self.name}':\n" + place_details

