class Date:
    def __init__(self, year: int, month: int, day: int):
        self.year = year
        self.month = month
        self.day = day


class User:
    def __init__(
        self,
        usr_id: int,
        birthdate: str,
        name: str,
        email: str,
    ):
        self.usr_id = usr_id
        self.birthdate = birthdate
        self.name = name
        self.email = email

    def __eq__(self, __value: object) -> bool:
        """Compare two Place objects for equality"""
        if isinstance(__value, User):
            # Compare all attributes for equality
            return (
                self.usr_id == __value.usr_id
                and self.name == __value.name
                and self.birthdate == __value.birthdate
                and self.email == __value.email
            )
        return False
