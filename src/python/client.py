import grpc
import travel_pb2
import travel_pb2_grpc


import grpc
import travel_pb2
import travel_pb2_grpc
from interfaces.travel_interface import TravelInterface


def get_place():
    """Get a place from the server"""
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = travel_pb2_grpc.TravelatorStub(channel)
        response = stub.get_place(travel_pb2.PlaceIdRequest(id=1))


def add_place():
    """Add a place to the server"""
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = travel_pb2_grpc.TravelatorStub(channel)

        name = "Hótel Transilvania"
        description = "Hotel de vampiros"
        location = "París, Francia"

        response = stub.add_place(
            travel_pb2.AddPlaceRequest(
                name=name, description=description, location=location
            )
        )
    if response.success:
        print(f"Place added successfully with ID: {response.id}")
    else:
        print("Failed to add place")


def run():
    get_place()
    add_place()


run()
