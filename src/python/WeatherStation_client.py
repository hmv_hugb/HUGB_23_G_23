import grpc
# Imports the grpc helper files for the weather service
from models.place import Location
import WeatherStation_pb2 as weatherStation_pb2
import WeatherStation_pb2_grpc as weatherStation_pb2_grpc


import datetime as dt

API_KEY = '81e6132b-b779-44d0-bf27-874475bc733a'
SERVICE_URL = 'hugb-service-ep3thvejnq-uc.a.run.app:443'

class weatherStationClient:
   def __init__(self):
      self.API_KEY = API_KEY
      self.SERVICE_URL = SERVICE_URL

   def get_current_weather(self, location:Location):
      try:
         with grpc.secure_channel(self.SERVICE_URL, grpc.ssl_channel_credentials()) as channel:
            stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
            current_weather = stub.get_current_weather(weatherStation_pb2.Location(lat=location.latitude, lon=location.longitude, api_key=API_KEY))
      except grpc.RpcError as e:
            raise Exception("Error: " + e.details())
      else:
         return current_weather
      
   def get_four_day_forecast(self, location:Location):
      try:
         with grpc.secure_channel(self.SERVICE_URL, grpc.ssl_channel_credentials()) as channel:
            stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
            four_day_forecast = stub.get_four_day_forecast(weatherStation_pb2.Location(lat=location.latitude, lon=location.longitude, api_key=API_KEY))
      except grpc.RpcError as e:
            raise Exception("Error: " + e.details())
      else:
         forecast_dict = {
            "temp": four_day_forecast.temp,
            "humidity": four_day_forecast.humidity,
            "clouds": four_day_forecast.clouds,
            "wind": four_day_forecast.wind,
            "windDeg": four_day_forecast.windDeg,
         }
         return forecast_dict
   
   def get_historical_forecast(self, location: Location):
      try:
         with grpc.secure_channel(self.SERVICE_URL, grpc.ssl_channel_credentials()) as channel:
            stub = weatherStation_pb2_grpc.WeatherStationStub(channel)
            historical_forecast = stub.get_historical_forecast(weatherStation_pb2.Location(lat=location.latitude, lon=location.longitude, api_key=API_KEY))
      except grpc.RpcError as e:
            raise Exception("Error: " + e.details())
      else:
         forecast_dict = {
            "temp": historical_forecast.temp,
            "humidity": historical_forecast.humidity,
            "clouds": historical_forecast.clouds,
            "wind": historical_forecast.wind,
            "windDeg": historical_forecast.windDeg,
         }
         return forecast_dict

x = weatherStationClient()
location_request = Location(latitude=64.13548,longitude=-21.89541)
print(x.get_current_weather(location_request))