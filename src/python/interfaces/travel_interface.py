from models.place import Place, Location
from models.itinerary import Itinerary
import json
import os
from typing import List
from typing import Optional


class TravelInterface:
    def __init__(self):
        # Going up one level from the current file and then down to the data folder
        data_folder = os.path.join(
            os.path.dirname(__file__), "..", "..", "data"
        )  # Initialize data folder
        self.places_file = os.path.join(
            data_folder, "places.json"
        )  # Initialize file paths
        # Initialize places list with items from the json file
        self.places: List[Place] = []
        self.load_places()
        self.itinerary = Itinerary("Default Itinerary", "2023-01-01", "2023-12-31")
        self.travel_history = []
        self.load_travel_history()

    def load_places(self):
        """Opens the places.json file and loads the data into the places list"""
        try:
            with open(self.places_file, "r", encoding="utf-8") as file:
                data = json.load(file)
                for place_data in data:
                    # Create a Place object from the JSON data
                    place = VotablePlace(
                        id=place_data["id"],
                        name=place_data["name"],
                        description=place_data["description"],
                        location=Location(
                            latitude=place_data["latitude"],
                            longitude=place_data["longitude"],
                        ),
                    )
                    place.votes = place_data.get("votes", 0)
                    # Append the Place object to the places list
                    self.places.append(place)
        except FileNotFoundError:
            raise Exception("The 'places.json' file could not be found.")

    def update_places_json(self):
        """Rewrites the places.json file with the data from the places list"""
        # Convert the list of Place objects to a list of dictionaries
        places_data = []
        for place in self.places:
            place_data = {
                "id": place.id,
                "name": place.name,
                "description": place.description,
                "latitude": place.location.latitude,
                "longitude": place.location.longitude,
            }
            places_data.append(place_data)

        # Write the updated data to the json file
        with open(self.places_file, "w", encoding="utf-8") as file:
            json.dump(places_data, file, indent=4)

    def get_place(self, place_id: int = 0):
        """Returns a Place object corresponding to the ID given as a parameter,
        otherwise returns None"""
        for place in self.places:
            if place.id == place_id:
                return place
        return None

    def add_place(self, name: str, description: str, location: Location):
        """Generates an id, creates a new Place object with given parameter
        values and appends the Place object to the places list"""
        new_id = len(self.places) + 1
        new_place = Place(new_id, name, description, location)
        self.places.append(new_place)
        self.update_places_json()
        return new_place

    def get_all_places(self):
        """Returns the places list"""
        return self.places

    def delete_place(self, place_id: int):
        """Deletes a place by its ID from the places list (created within the class) and the JSON file"""

        place_to_delete = None  # Initialize a variable to store the place to be deleted
        for place in self.places:
            if place.id == place_id:  # Check if the place's ID matches the given ID
                place_to_delete = place  # Set the place to be deleted
                break  # Exit the loop if we found the place to delete

        if place_to_delete:
            # Remove the place from the places list
            self.places.remove(place_to_delete)
            self.update_places_json()  # Update the JSON file after removing the place
            print(f"Place with ID {place_id} has been deleted.")
        else:
            print(f"No place found with ID {place_id}. Nothing deleted.")

    def edit_place(
        self,
        place_id: int,
        name: Optional[str] = None,
        description: Optional[str] = None,
        location: Optional[Location] = None,
    ):
        """Edits the Place object with the given ID (if it exists) and updates the places.json file
        Returns an exception if the place does not exist, otherwise the updated place"""
        place = self.get_place(place_id)
        if place is None:
            raise Exception(f"Place with id {place_id} does not exist.")
        if name is not None:
            place.name = name
        if description is not None:
            place.description = description
        if location is not None:
            place.location = location
        self.update_places_json()
        return place

    def upvote_place(self, place_id: int):
        """Adds one vote to the place with the given ID"""
        place = self.get_place(place_id)
        if place is None:
            raise Exception(f"Place with id {place_id} does not exist.")
        if isinstance(place, VotablePlace):
            place.upvote()
            self.update_places_json()
            return place
        else:
            raise Exception(f"Place with id {place_id} is not votable.")

    def downvote_place(self, place_id: int):
        """Removes one vote from the place with the given ID"""
        place = self.get_place(place_id)
        if place is None:
            raise Exception(f"Place with id {place_id} does not exist.")
        if isinstance(place, VotablePlace):
            place.downvote()
            self.update_places_json()
            return place
        else:
            raise Exception(f"Place with id {place_id} is not votable.")

    def add_place_to_itinerary(self, place_id: int):
        place = self.get_place(place_id)
        if place:
            self.itinerary.add_place(place)
            print(f"Added {place.name} to the itinerary!")
        else:
            print(f"No place found with ID {place_id}. Nothing added.")

    def remove_place_from_itinerary(self, place_id: int):
        if self.itinerary.remove_place(place_id):
            print(f"Removed place with ID {place_id} from the itinerary!")
        else:
            print(
                f"No place found in the itinerary with ID {place_id}. Nothing removed."
            )

    def list_itinerary(self):
        print("Current Itinerary:")
        if not self.itinerary.places:
            print("No places added to the itinerary yet.")
        else:
            for place in self.itinerary.list_places():
                print(place)

    def rate_place(self, place_id: int, user_id: int, rating: int, review: str = None):
        """Adds a rating to a place"""
        place = self.get_place(place_id)
        if place is None:
            raise Exception(f"Place with id {place_id} does not exist.")
        place.add_rating(user_id, rating, review)
        self.update_places_json()
        return place

    """ Functionality to make users be able to get suggested places based on their travel history"""

    def load_travel_history(self):
        try:
            with open("travel_history.json", "r", encoding="utf-8") as file:
                self.travel_history = json.load(file)
        except FileNotFoundError:
            self.travel_history = []

    def get_visited_places(self):
        return [self.get_place(place_id) for place_id in self.travel_history]

    def suggest_places(self):
        visited_place_ids = set(self.travel_history)
        suggestions = [
            place for place in self.places if place.id not in visited_place_ids
        ]
        return suggestions

    def get_suggested_places(self):
        return self.suggest_places()

    """ Function to share itinerary on social media"""

    def share_on_social_media(self, shareable_id: int, shareable_type: str = "place"):
        """Simulates sharing a place or itinerary on social media."""
        if shareable_type == "place":
            place = self.get_place(shareable_id)
            if place is not None:
                content = f"Check out this place: {place.name} - {place.description}"
            else:
                raise Exception("The place does not exist.")
        elif shareable_type == "itinerary":
            content = "Check out my travel itinerary: " + ", ".join(
                [place.name for place in self.itinerary.list_places()]
            )
        else:
            raise ValueError("Invalid shareable type. Choose 'place' or 'itinerary'.")

        # Simulate sharing on social media by returning the string
        print(f"Shared on social media: {content}")
        return content


class VotablePlace(Place):
    def __init__(
        self, id: int, name: str, description: str, location: Location, votes: int = 0
    ):
        """A place that can be voted on"""
        super().__init__(id, name, description, location)
        self.votes = votes

    def upvote(self):
        """Adds one vote to the place"""
        self.votes += 1

    def downvote(self):
        """Removes one vote from the place, if the place has no votes, nothing happens"""
        if self.votes > 0:
            self.votes -= 1

    def __str__(self):
        """Returns a string representation of the place with its votes"""
        return f"{super().__str__()} Votes: {self.votes}"
