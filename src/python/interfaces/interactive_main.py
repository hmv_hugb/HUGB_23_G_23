from ..models.user import User
from ..models.itinerary import Itinerary

from ..interfaces.user_interface import UserInterface
from ..interfaces.travel_interface import TravelInterface

def main():
    travel_interface = TravelInterface()


    user_interface = UserInterface()

    while True:
        print("\n--- Main Menu ---")
        print("1. Manage Places")
        print("2. Manage Users")
        print("3. Exit")
        choice = input("Choose an option: ")

        if choice == "1":
            while True:
                print("\n--- Places Menu ---")
                print("1. Load Places")
                print("2. Add Place")
                print("3. Delete Place")
                print("4. Edit Place")
                print("5. List All Places")
                print("6. Back to Main Menu")
                choice_places = input("Choose an option: ")

                if choice_places == "1":
                    travel_interface.load_places()
                    print("Loaded places successfully!")
                elif choice_places == "2":
                    # Simplified example. In a real-world scenario, you'd ask for all required fields.
                    name = input("Enter place name: ")
                    description = input("Enter place description: ")
                    # Location would also require inputs for latitude, longitude, etc.
                    location = Location(0, 0)  # Dummy values for now
                    travel_interface.add_place(name, description, location)
                elif choice_places == "3":
                    place_id = int(input("Enter place ID to delete: "))
                    travel_interface.delete_place(place_id)
                elif choice_places == "4":
                    place_id = int(input("Enter place ID to edit: "))
                    # Simplified example. In a real-world scenario, you'd ask for all fields that can be edited.
                    name = input("Enter new place name: ")
                    travel_interface.edit_place(place_id, name=name)
                elif choice_places == "5":
                    for place in travel_interface.get_all_places():
                        print(place)
                elif choice_places == "6":
                    break

        elif choice == "2":
            while True:
                print("\n--- Users Menu ---")
                print("1. Load Users")
                print("2. Add User")
                print("3. Delete User")
                print("4. Edit User")
                print("5. List All Users")
                print("6. Back to Main Menu")
                choice_users = input("Choose an option: ")

                if choice_users == "1":
                    user_interface.load_users()
                    print("Loaded users successfully!")
                elif choice_users == "2":
                    name = input("Enter user name: ")
                    birthdate = input("Enter user birthdate: ")
                    email = input("Enter user email: ")
                    user_interface.add_user(name, birthdate, email)
                elif choice_users == "3":
                    usr_id = int(input("Enter user ID to delete: "))
                    user_interface.delete_user(usr_id)
                elif choice_users == "4":
                    usr_id = int(input("Enter user ID to edit: "))
                    name = input("Enter new user name: ")
                    user_interface.edit_user(usr_id, name=name)
                elif choice_users == "5":
                    for user in user_interface.users:
                        print(user)
                elif choice_users == "6":
                    break

        elif choice == "3":
            print("Exiting program. Goodbye!")
            break


if __name__ == "__main__":
    main()
