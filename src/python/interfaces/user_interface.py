from models.user import User
from models.itinerary import Itinerary
import json
import os
from typing import List
from typing import Optional


class UserInterface:
    def __init__(self):
        # Going up one level from the current file and then down to the data folder
        data_folder = os.path.join(os.path.dirname(__file__), "..", "..", "data")
        self.users_file = os.path.join(data_folder, "users.json")
        # Initialize places list with items from the json file
        self.users: List[User] = []
        self.load_users()

    def load_users(self):
        """Opens the users.json file and loads the data into the users list"""
        try:
            with open(self.users_file, "r", encoding="utf-8") as file:
                data = json.load(file)
                for user_data in data:
                    # Create a Place object from the data
                    user = User(
                        user_data["usr_id"],
                        user_data["birthdate"],
                        user_data["name"],
                        user_data["email"],
                    )
                    # Append the Place object to the places list
                    self.users.append(user)
        except FileNotFoundError:
            raise Exception("The 'users.json' file could not be found.")

    def update_users_json(self):
        """Rewrites the users.json file with the data from the users list"""
        # Convert the list of Place objects to a list of dictionaries
        users_data = []
        for user in self.users:
            user_data = {
                "usr_id": user.usr_id,
                "birthdate": user.birthdate,
                "name": user.name,
                "email": user.email,
            }
            users_data.append(user_data)

        # Write the updated data to the json file
        with open(self.users_file, "w", encoding="utf-8") as file:
            json.dump(users_data, file, indent=4)

    def get_user(self, usr_id: int = 0):
        """Returns a Place object corresponding to the ID given as a parameter,
        otherwise returns None"""
        for user in self.users:
            if user.usr_id == usr_id:
                return user
        return None

    def add_user(self, name: str, birthdate: str, email: str):
        """Generates an id, creates a new Place object with given parameter
        values and appends the Place object to the places list"""
        new_id = len(self.users) + 1
        new_user = User(new_id, birthdate, name, email)
        self.users.append(new_user)
        self.update_users_json()
        return new_user

    def delete_user(self, usr_id: int):
        """Deletes a place by its ID from the places list (created within the class) and the JSON file"""

        user_to_del = None  # Initialize a variable to store the place to be deleted
        for usr in self.users:
            if usr.usr_id == usr_id:  # Check if the place's ID matches the given ID
                user_to_del = usr  # Set the place to be deleted
                break  # Exit the loop if we found the place to delete

        if user_to_del:
            # Remove the place from the places list
            self.users.remove(user_to_del)
            self.update_users_json()  # Update the JSON file after removing the place

    def edit_user(
        self,
        usr_id: int,
        name: Optional[str] = None,
        birthdate: Optional[str] = None,
        email: Optional[str] = None,
    ):
        """Edits the Place object with the given ID (if it exists) and updates the places.json file
        Returns an exception if the place does not exist, otherwise the updated place"""
        usr = self.get_user(usr_id)
        if usr is None:
            raise Exception(f"Place with id {usr_id} does not exist.")
        if name is not None:
            usr.name = name
        if birthdate is not None:
            usr.birthdate = birthdate
        if email is not None:
            usr.email = email
        self.update_users_json()
        return usr
